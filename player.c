#include "bot.h"

int myx = 0;
int myy = 0;
int dist(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}
struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;

    // Copy player's x-y to globals
    myx = me.x;
    myy = me.y;
    
    // Sort food by distance
    qsort(foods, nfood, sizeof(struct food), dist);
    
    // Move toward closest food
    act.dx = foods[0].x - me.x;
    act.dy = foods[0].y - me.y;
    act.fire = 0;
    act.split = 0;
    
    return act;
}
